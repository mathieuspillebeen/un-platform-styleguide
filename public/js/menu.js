var menuFunctionality = {
  init: function() {
    var hash = window.top.location.hash.substr(1);

    setActiveLink(hash);
    this.events();
  },

  events: function() {
    let titles = document.querySelectorAll(".main-layout__right *[id]");
    titles.forEach(title => {
       // Create anchor element. 
       var a = document.createElement('a');  
                  
       // Create the text node for anchor element. 
       var link = document.createTextNode("#"); 
         
       // Append the text node to anchor element. 
       a.appendChild(link);  
         
       // Set the href property. 
       a.href = "#" + title.id;  

       // Append the anchor element to the body. 
       title.appendChild(a);
    });
    
    var scrollpositions = $('.main-layout__right *[id]');
    // Add a scrolllistener
    $(window).scroll(function(){
      // Get container scroll position
      var fromTop = $(this).scrollTop();
   
      // // Get id of current scroll item
      var cur = scrollpositions.map(function(){
        if ($(this).offset().top < fromTop + 150)
          return this;
      });


      // Get the id of the current element
      cur = cur[cur.length-1];
      var id = cur && cur.length ? cur[0].id : "";
      if(cur) {
        setActiveLink(cur.id);
      }
    });
  }
};

function setActiveLink(hash) {
  $('.nav-list--first-level a.active').removeClass('active');
  
  let $active_link = $('.nav-list--first-level a[href="#' + hash + '"]');

  // Give the deepest link an active state.
  $active_link.addClass('active js-open');
  
  // Give the deepest link an active state.
  let $parent_link = $active_link.closest('.nav-list').siblings('a');
  $parent_link.addClass('active js-open');

  // Give the deepest link an active state.
  let $parent_parent_link = $parent_link.closest('.nav-list').siblings('a');
  $parent_parent_link.addClass('active js-open');

  // Give the deepest link an active state.
  let $parent_parent_parent_link = $parent_parent_link.closest('.nav-list').siblings('a');
  $parent_parent_parent_link.addClass('active js-open');
};

$(window).load(function() {
  menuFunctionality.init();
});

