var codeSnippets = {
  init: function() {
    this.codeToCreateSnippetClass = '.snippet';
    this.codeSnippetsClass = '.code-toggle';

    this.codeSnippetsSetup();
    this.events();
  },

  events: function() {
    var _this = this;

    $(this.codeSnippetsClass).click(function(e) {
      e.preventDefault();
      var $code = $(this).siblings('.language-markup, .example');

      $code.toggle();
    });
  },

  /**
   * Setup markup code snippet.
   * It gets the HTML of the element and creates the code area
   */
  codeSnippetsSetup: function() {
    var _this = this,
      options = {
        "indent":"auto",
        "indent-spaces":2,
        "wrap":80,
        "markup":true,
        "output-xml":false,
        "numeric-entities":true,
        "quote-marks":true,
        "quote-nbsp":false,
        "show-body-only":true,
        "quote-ampersand":false,
        "break-before-br":true,
        "uppercase-tags":false,
        "uppercase-attributes":false,
        "drop-font-tags":true,
        "tidy-mark":false,
        "quiet":"yes",
        "show-warnings":"no"
      };

    $(this.codeToCreateSnippetClass).each(function(i, obj) {
      var snippetClassName = _this.codeToCreateSnippetClass.replace(/\./g, '');
      var snippet = tidy_html5($(obj).get(0).outerHTML.replace(' ' + snippetClassName, '').replace(snippetClassName, ''), options);

      // $(obj).before('<a href="#" class="' + _this.codeSnippetsClass.replace('.', '') + '"></a>');
      $(obj).after('<pre class="language-markup"><code>' + $('<p/>').text(snippet).html() + '</code></pre>').next().hide();
    });

    Prism.highlightAll();
  }
};

$(window).load(function() {
  codeSnippets.init();
});

